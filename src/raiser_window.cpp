#include "raiser_window.h"

#if defined(_apple_) || defined(__macintosh__)
#include "raiser_window_impl_mac.h"
#elif defined(_WIN32) || defined(_WIN64) || defined(_WIN)
#include "raiser_window_impl_win.h"
#elif defined(__unix__) || defined(__linux__)
#include "raiser_window_impl_unix.h"
#endif


RaiserWindow::RaiserWindow() {
    raiser_impl = new RaiserWindowImpl();
}

RaiserWindow::~RaiserWindow() {
    delete raiser_impl;
}

void RaiserWindow::Show(unsigned long long Pid) {
    raiser_impl->ShowImpl(Pid);
}