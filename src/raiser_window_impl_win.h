#ifndef PID_RESHOW_RAISER_WINDOW_IMPL_WIN_H
#define PID_RESHOW_RAISER_WINDOW_IMPL_WIN_H

#include "windows.h"

struct RaiserWindowImpl {
    void ShowImpl(unsigned long long process_id);

    static BOOL IsMainWindow(HWND handle);

    static BOOL CALLBACK EnumWindowsCallback(HWND, LPARAM);

private:
    HWND GetMainWindow();

    BOOL CALLBACK EnumWindowsProc();

    struct handle_data {
        unsigned long process_id;
        HWND window_handle;
    } _data;
};


#endif //PID_RESHOW_RAISER_WINDOW_IMPL_WIN_H
