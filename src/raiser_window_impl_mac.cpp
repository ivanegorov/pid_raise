#include "raiser_window_impl_mac.h"

#include "maximize_mac_impl.h"

void RaiserWindowImpl::ShowImpl(unsigned long long process_id) {
    macos_force_maximize_window(process_id);
}