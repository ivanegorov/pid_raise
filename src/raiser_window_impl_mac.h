#ifndef PID_RESHOW_RAISER_WINDOW_IMPL_MAC_H
#define PID_RESHOW_RAISER_WINDOW_IMPL_MAC_H

#include <stdio.h>
#include <cstdlib>

struct RaiserWindowImpl {
    static void ShowImpl(unsigned long long process_id);
};

#endif //PID_RESHOW_RAISER_WINDOW_IMPL_MAC_H
