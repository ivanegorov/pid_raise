#ifndef PID_RESHOW_MAXIMIZE_MAC_IMPL_H
#define PID_RESHOW_MAXIMIZE_MAC_IMPL_H

#ifdef __cplusplus
extern "C" {
#endif

void macos_force_maximize_window(int pid);

#ifdef __cplusplus
}
#endif

#endif //PID_RESHOW_MAXIMIZE_MAC_IMPL_H
