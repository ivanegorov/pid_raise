#include "raiser_window_impl_win.h"

void RaiserWindowImpl::ShowImpl(unsigned long long process_id) {
    _data.process_id = process_id;
    _data.window_handle = nullptr;
    EnumWindowsProc();
}

BOOL RaiserWindowImpl::IsMainWindow(HWND handle) {
    return GetWindow(handle, GW_OWNER) == (HWND)nullptr && IsWindowVisible(handle);
}

HWND RaiserWindowImpl::GetMainWindow() {
    EnumWindows(RaiserWindowImpl::EnumWindowsCallback, (LPARAM)&_data);
    return _data.window_handle;
}

BOOL RaiserWindowImpl::EnumWindowsCallback(HWND handle, LPARAM lparam) {
    handle_data& data = *(handle_data*)lparam;
    unsigned long process_id = 0;
    GetWindowThreadProcessId(handle, &process_id);
    if (data.process_id != process_id || !IsMainWindow(handle))
        return TRUE;
    data.window_handle = handle;
    return FALSE;
}

BOOL RaiserWindowImpl::EnumWindowsProc() {
    HWND hWnd = GetMainWindow();
    DWORD dwProcessId;
    GetWindowThreadProcessId(hWnd, &dwProcessId);
    if (dwProcessId == (DWORD)(LPARAM)_data.process_id) {
        if (IsIconic(hWnd))
            ShowWindow(hWnd, SW_RESTORE);
        SetForegroundWindow(hWnd);
    }
    return TRUE;
}