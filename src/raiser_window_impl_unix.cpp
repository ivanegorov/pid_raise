#include "raiser_window_impl_unix.h"

WindowsMatchingPid::WindowsMatchingPid(Display *display, Window wRoot, unsigned long pid)
        : _display(display), _pid(pid) {
    _atomPID = XInternAtom(display, "_NET_WM_PID", True);
    if (_atomPID == None) {
        return;
    }

    search(wRoot);
}

void WindowsMatchingPid::search(Window w) {
    Atom type;
    int format;
    unsigned long nItems;
    unsigned long bytesAfter;
    unsigned char *propPID = 0;
    if (Success == XGetWindowProperty(_display, w, _atomPID, 0, 1, False, XA_CARDINAL,
                                      &type, &format, &nItems, &bytesAfter, &propPID)) {
        if (propPID != 0) {
            if (_pid == *((unsigned long *) propPID))
                _result.push_back(w);

            XFree(propPID);
        }
    }

    Window wRoot;
    Window wParent;
    Window *wChild;
    unsigned nChildren;
    if (0 != XQueryTree(_display, w, &wRoot, &wParent, &wChild, &nChildren)) {
        for (unsigned i = 0; i < nChildren; i++)
            search(wChild[i]);
    }
}

void RaiserWindowImpl::ShowImpl(unsigned long long process_id) {
    WindowsMatchingPid match(display, XDefaultRootWindow(display), process_id);

    const std::list<Window> &result = match.result();
    for (std::list<Window>::const_iterator it = result.begin(); it != result.end(); it++) {
        XEvent event = {0};
        event.xclient.type = ClientMessage;
        event.xclient.serial = 0;
        event.xclient.send_event = True;
        event.xclient.message_type = XInternAtom(display, "_NET_ACTIVE_WINDOW", True);
        event.xclient.window = *it;
        event.xclient.format = 32;

        XSendEvent(display, RootWindow(display, XDefaultScreen(display)), False, SubstructureRedirectMask | SubstructureNotifyMask,
                   &event);
        XMapRaised(display, *it);
    }
}