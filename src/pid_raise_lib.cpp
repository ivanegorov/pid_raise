#include <node.h>

#include "raiser_window.h"

using v8::Exception;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::String;
using v8::Value;

void Awaken(const FunctionCallbackInfo <Value> &args)  {
    static RaiserWindow rw;

    Isolate* isolate = args.GetIsolate();

    if (args.Length() < 1) {
        isolate->ThrowException(Exception::TypeError(
                String::NewFromUtf8(isolate,
                                    "Wrong number of arguments, Usage: pid_raise <PID>").ToLocalChecked()));
        return;
    }

    if (!args[0]->IsNumber()) {
        isolate->ThrowException(Exception::TypeError(
                String::NewFromUtf8(isolate,
                                    "Wrong arguments").ToLocalChecked()));
        return;
    }

    rw.Show(args[0].As<Number>()->Value());
}

NODE_MODULE_INIT(){
    NODE_SET_METHOD(exports, "awaken", Awaken);
}