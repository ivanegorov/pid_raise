#include <iostream>
#include <string>
#include "src/raiser_window.h"

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: pid_raise <PID>\n" << std::endl;
        return 1;
    }

    RaiserWindow rw;

    unsigned long long dwProcessPid = std::stoll(argv[1]); // Your PID
    rw.Show(dwProcessPid);

    return 0;
}