#include "maximize_mac_impl.h"

#import <Foundation/Foundation.h>
#import <Carbon/Carbon.h>
#import <AppKit/AppKit.h>

#import <Cocoa/Cocoa.h>
#import <libproc.h>
#import <string.h>
#import <stdlib.h>
#import <stdio.h>

void macos_force_maximize_window(int pid) {
    NSDictionary *options = @{(id)kAXTrustedCheckOptionPrompt: @YES};
    BOOL accessibilityEnabled = AXIsProcessTrustedWithOptions((CFDictionaryRef)options);

    if (accessibilityEnabled) {
        NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
        NSArray *activeApps = [workspace runningApplications];

        AXUIElementRef aue = AXUIElementCreateApplication ((int)pid);

        CFArrayRef windowList;
        AXUIElementCopyAttributeValue(aue,  kAXWindowsAttribute, (CFTypeRef *) &windowList);
        if (CFArrayGetCount(windowList) > 0) {
            AXUIElementRef windowRef = (AXUIElementRef) CFArrayGetValueAtIndex(windowList, 0);
            AXUIElementSetAttributeValue(windowRef, kAXMinimizedAttribute, kCFBooleanFalse);
        }

        for (NSRunningApplication *app in activeApps) {
            if ([app processIdentifier] == (pid_t) pid) {
                [app unhide];
                [app activateWithOptions:NSApplicationActivateIgnoringOtherApps | NSApplicationActivateAllWindows];
            }
        }
    }
}

