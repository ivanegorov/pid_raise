#ifndef PID_RESHOW_RAISER_WINDOW_H
#define PID_RESHOW_RAISER_WINDOW_H

#include <memory>

struct RaiserWindow {
public:
    RaiserWindow();
    ~RaiserWindow();
    void Show(unsigned long long Pid);
private:
    struct RaiserWindowImpl * raiser_impl;
};


#endif //PID_RESHOW_RAISER_WINDOW_H
