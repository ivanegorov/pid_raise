#ifndef PID_RESHOW_RAISER_WINDOW_IMPL_UNIX_H
#define PID_RESHOW_RAISER_WINDOW_IMPL_UNIX_H

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <list>

class WindowsMatchingPid {
public:
    WindowsMatchingPid(Display *display, Window wRoot, unsigned long pid);

    const std::list<Window> &result() const { return _result; }

private:
    unsigned long _pid;
    Atom _atomPID;
    Display *_display;
    std::list<Window> _result;

    void search(Window w);
};

struct RaiserWindowImpl {
public:
    RaiserWindowImpl() : display(XOpenDisplay(nullptr)) {}

    ~RaiserWindowImpl() { XCloseDisplay(display); }

    void ShowImpl(unsigned long long pid);

private:
    Display *display{};
};


#endif //PID_RESHOW_RAISER_WINDOW_IMPL_UNIX_H
